Class {
	#name : #Zettelkasten,
	#superclass : #Object,
	#instVars : [
		'rawData'
	],
	#category : #'Zettelkasten-Model'
}

{ #category : #accessing }
Zettelkasten class >> for: aFileFolder [
	^ self new
		rawData: aFileFolder;
		yourself
]

{ #category : #accessing }
Zettelkasten >> rawData [
	^ rawData
]

{ #category : #accessing }
Zettelkasten >> rawData: anObject [
	rawData := anObject
]

{ #category : #accessing }
Zettelkasten >> zettelFiles [
	^ rawData allChildrenMatching: '*.md'
]

{ #category : #accessing }
Zettelkasten >> zettels [
	^ self zettelFiles collect: [ :f | Zettel for: f ]
]
