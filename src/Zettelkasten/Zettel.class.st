Class {
	#name : #Zettel,
	#superclass : #Object,
	#instVars : [
		'rawData'
	],
	#category : #'Zettelkasten-Model'
}

{ #category : #accessing }
Zettel class >> for: aFileReference [
	^ self new
		rawData: aFileReference;
		yourself
]

{ #category : #accessing }
Zettel >> rawData [
	^ rawData
]

{ #category : #accessing }
Zettel >> rawData: anObject [
	rawData := anObject
]
