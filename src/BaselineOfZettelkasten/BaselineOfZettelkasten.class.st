Class {
	#name : #BaselineOfZettelkasten,
	#superclass : #BaselineOf,
	#category : #BaselineOfZettelkasten
}

{ #category : #loading }
BaselineOfZettelkasten class >> loadLepiter [
	IceRepository registry  
		detect: [ :aRepository |
			aRepository name = 'zettelkasten' ]
		ifFound: [ :aRepository | 
			| defaultDatabase currentProperties |
			defaultDatabase := LeDatabasesRegistry defaultLogicalDatabase.
			currentProperties := defaultDatabase properties.
			currentProperties addRegisteredDirectory: aRepository repositoryDirectory / 'lepiter'.
			defaultDatabase reload ]
		ifNone: [
			self inform: 'Repository not found.' ]
]

{ #category : #baseline }
BaselineOfZettelkasten >> baseline: spec [
	<baseline>
		^ spec for: #common do: [
			spec package: 'Zettelkasten'
		]
]
